import * as React from 'react';
import { connect } from 'react-redux';
import Home from './scenes/Home';
import NotFound from './scenes/NotFound';
import SinglePage from './scenes/SinglePage';

const pages = {
  Cognate: SinglePage,
  Home,
  NotFound
};

const App = ({ page }) => {
  const Component = pages[page];
  return <Component />;
};

const mapStateToProps = ({ page }) => ({ page });

export default connect(mapStateToProps)(App);
