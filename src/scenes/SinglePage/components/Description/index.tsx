import * as React from 'react';
import { connect } from 'react-redux';
import * as selectors from '../../../../reducers/cognateReducer';
import './styles.scss';

interface InterfaceProps {
  lexicalEntries: object[][];
}

const Description: React.SFC<InterfaceProps> = ({ lexicalEntries }) => {
  if (lexicalEntries === undefined) {
    return null;
  }

  const { entries } = lexicalEntries[0];
  const { senses } = entries[0];

  return (
    <div className='description'>
      <h2 className='description__headline'>Description</h2>
      {lexicalEntries &&
        senses.map((entry, index) => {
          return (
            <div className='description__data' key={entry.id}>
              <p className='description__text'>{`${index + 1}. ${
                entry.definitions[0]
              }`}</p>
              {entry.examples && <h3>Examples:</h3>}
              {entry.examples &&
                entry.examples.map(example => {
                  return (
                    <p key={example.text}>
                      <i>{example.text}</i>
                    </p>
                  );
                })}
            </div>
          );
        })}
    </div>
  );
};

const mapStateToProps = state => ({
  lexicalEntries: selectors.activeCognateSelector(state).en.lexicalEntries
});

const ConnectedDescription = connect(mapStateToProps)(Description);

export default ConnectedDescription;
