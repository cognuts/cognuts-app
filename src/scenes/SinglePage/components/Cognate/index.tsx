import * as React from 'react';
import { connect } from 'react-redux';
import { CSSTransition } from 'react-transition-group';
import Link from 'redux-first-router-link';
import * as selectors from '../../../../reducers/cognateReducer';
import Arrow from './components/Arrow';
import CognateText from './components/CognateText';
import './styles.scss';

interface ICognate {
	en: { id: string };
	pl: { id: string };
}
interface IProps {
	cognate: ICognate;
	languages: string[];
	nextCognate: ICognate;
	prevCognate: ICognate;
	state?: object;
}
interface IState {
	isPlaying: boolean;
	isVisible: boolean;
	direction: string;
}
class Cognate extends React.Component<IProps, IState> {
	constructor(props: IProps) {
		super(props);
		this.state = { isPlaying: false, isVisible: true, direction: 'next' };
	}

	public componentDidMount() {
		/* tslint:disable */ console.log(this.props); /* tslint:enable */
	}

	public componentDidUpdate = (prevProps: IProps, prevState: IState) => {
		if (prevProps.cognate !== this.props.cognate) {
			this.setState(() => ({
				isPlaying: false,
				isVisible: true
			}));
		}
	};

	public playCognate = (cognate: any): void => {
		if (
			cognate.en.lexicalEntries !== undefined &&
			this.state.isPlaying === false
		) {
			this.setState(() => ({
				isPlaying: true
			}));
			const audioUrl = cognate.en.lexicalEntries[0].pronunciations[0].audioFile;
			const audio = new Audio(audioUrl);
			audio.play();
			audio.onended = () => this.setState(() => ({ isPlaying: false }));
		}
	};

	public changeCognate = (direction: string) => {
		this.setState({
			direction,
			isVisible: false
		});
	};

	public render() {
		const { cognate, languages, nextCognate, prevCognate } = this.props;
		const { isVisible, direction } = this.state;
		return (
			<div className='cognate'>
				<h2>Cognate</h2>
				<div className={`cognate__wrapper ${direction}`}>
					{prevCognate ? (
						<Link
							to={{ type: 'COGNATE', payload: { id: prevCognate.en.id } }}
							onClick={() => this.changeCognate('prev')}
						>
							<Arrow direction='left' />
						</Link>
					) : null}
					<CSSTransition
						timeout={{ enter: 500, exit: 500 }}
						classNames='slide'
						in={isVisible}
					>
						<div
							className='cognate__words'
							onClick={() => this.playCognate(cognate)}
						>
							<div>
								{languages.map(language => (
									<CognateText
										text={cognate[language].id}
										key={`${cognate[language].id}`}
									/>
								))}
							</div>
						</div>
					</CSSTransition>
					{nextCognate ? (
						<Link
							to={{ type: 'COGNATE', payload: { id: nextCognate.en.id } }}
							onClick={() => this.changeCognate('next')}
						>
							<Arrow direction='right' />
						</Link>
					) : null}
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	cognate: selectors.findByIdSelector(state),
	languages: selectors.languagesSelector(state),
	nextCognate: selectors.nextCognateSelector(state),
	prevCognate: selectors.prevCognateSelector(state)
});

// const mapDispatchToProps = dispatch => ({
//     fetch: (language, id) => dispatch(fetchCognate(language, id)),
// })

const ConnectedCognate = connect(
	mapStateToProps
	// mapDispatchToProps
)(Cognate);

export default ConnectedCognate;
