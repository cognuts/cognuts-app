import * as React from 'react';

interface InterfaceProps {
  text: string;
}

const CognateText: React.SFC<InterfaceProps> = ({ text }) => (
  <p className='cognate__text'>{text}</p>
);

export default CognateText;