import * as React from 'react';
import './styles.scss';

interface InterfaceProps {
  direction: string;
}

const Arrow: React.SFC<InterfaceProps> = props => {
  return (
    <div className={`arrow arrow--${props.direction}`}>
      {props.direction === 'left' ? (
        <span className='arrow__char-wrapper'>{'<'}</span>
      ) : (
        <span className='arrow__char-wrapper'>{'>'}</span>
      )}
    </div>
  );
};

export default Arrow;
