import * as React from 'react';

interface IProps {
	type: string;
	text: string;
}

const ShareButton: React.SFC<IProps> = ({ type, text }) => {
  let base = '';
  switch (type) {
    case 'facebook':
      base = 'http://www.facebook.com/sharer.php?u='
      break;
    case 'twitter':
      base = 'https://twitter.com/intent/tweet?&url='
      break;
  }

	return (
		<a
			href={`${base}${document.URL}`}
			target='_blank'
			className={`share__button share__button--${type}`}
		>
			{text}
		</a>
	);
};

export default ShareButton;
