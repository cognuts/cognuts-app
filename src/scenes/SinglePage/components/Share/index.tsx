import * as React from 'react';
import ShareButton from './components/ShareButton';
import './styles.scss';

const Share: React.SFC = () => (
  <div className='share'>
    <ShareButton type='facebook' text='Udostępnij na Facebooku' />
    <ShareButton type='twitter' text='Udostępnij na Twitterze' />
  </div>
);

export default Share;
