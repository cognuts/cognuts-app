import Footer from 'components/Footer';
import Header from 'components/Header';
import * as React from 'react';
import Cognate from './components/Cognate';
import Description from './components/Description';
import Share from './components/Share';

class SinglePage extends React.Component {
	public render() {
		return (
			<>
				<Header />
				<Cognate />
				<Description />
				<Share />
				<Footer />
			</>
		);
	}
}

export default SinglePage;