import * as React from 'react';
import Link from 'redux-first-router-link';
import './styles.scss';

class Home extends React.Component {
  public render() {
    return (
      <div className='home'>
        <h1>Welcome to Cognuts</h1>
        <Link to={{ type: 'COGNATE' , payload: { id: 'absence' }}}>
          <button className='home__button'>Let's start</button>
        </Link>
      </div>
    );
  }
}

export default Home;
