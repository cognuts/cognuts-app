import * as React from 'react';
import Link from 'redux-first-router-link';
import './styles.scss';

class NotFound extends React.Component {
  public render() {
    return (
      <div className='home'>
        <h1>Not Found</h1>
        <Link to={{ type: 'COGNATE' , payload: { id: 'absence' }}}>
          <button className='home__button'>Back to Cognates</button>
        </Link>
      </div>
    );
  }
}

export default NotFound;
