import throttle from 'lodash/throttle';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import App from './App';
import configureStore from './configureStore';
import './index.scss';
import registerServiceWorker from './registerServiceWorker';
import { loadState, saveState, updateCognates } from './services/localStorage';

const persistedState = updateCognates(loadState());

export const store = configureStore(persistedState);

store.subscribe(
	throttle(() => {
		saveState({
			cognates: store.getState().cognates
		});
	}, 1000)
);

ReactDOM.render(
	<Provider store={store}>
		<App />
	</Provider>,
	document.getElementById('root') as HTMLElement
);
registerServiceWorker();
