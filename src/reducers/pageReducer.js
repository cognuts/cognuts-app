import { NOT_FOUND } from 'redux-first-router'

const components = {
    COGNATE: 'Cognate',
    HOME: 'Home',
    [NOT_FOUND]: 'NotFound',
  }
  
  export default (state = 'HOME', action = {}) => components[action.type] || state;