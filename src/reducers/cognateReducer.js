import { combineReducers } from 'redux';
import * as actionTypes from '../actions/types';
import words from '../resources/words';
import { createSelector } from 'reselect'

const initialState = words;

function data(state = initialState, action) {
  switch (action.type) {
    case actionTypes.FETCH_COGNATE_SUCCESS:
      const result = action.payload.response.results[0];
      const id = result.id;
      const language = result.language;
      const cognateIndex = state.findIndex(c => c.en.id === id);
      const cognateObject = state[cognateIndex];
      const updatedCognate = {
        ...cognateObject,
        [language]: {
          ...result
        }
      };
      return Object.assign([], state, {[cognateIndex]: updatedCognate});

    default:  
      return state
  }
}

function languages(state = ['en','pl'], action) {
  switch (action.type) {
    default:
      return state
  }
}

function missionControl(state = {}, action) {
  switch (action.type) {
    default:
      return state
  }
}

const cognateReducer = combineReducers({
  data,
  languages,
  missionControl,
})

export default cognateReducer;

export const cognatesSelector = state => state.cognates.data;
export const languagesSelector = state => state.cognates.languages;
export const findIndexByIdSelector = state => state.cognates.data.findIndex(c => c.en.id === state.location.payload.id);
export const findByIdSelector = state => state.cognates.data.find(c => c.en.id === state.location.payload.id);

export const activeCognateSelector = createSelector(
  cognatesSelector,
  findIndexByIdSelector,
  (stateCognates,activeIndex) => stateCognates[activeIndex]
)

export const prevCognateSelector = createSelector(
  cognatesSelector,
  findIndexByIdSelector,
  (stateCognates,activeIndex) => stateCognates[activeIndex-1]
)

export const nextCognateSelector = createSelector(
  cognatesSelector,
  findIndexByIdSelector,
  (stateCognates,activeIndex) => stateCognates[activeIndex+1]
)