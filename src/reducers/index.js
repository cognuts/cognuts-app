import cognateReducer from './cognateReducer';
import pageReducer from './pageReducer';
import { default as title } from './titleReducer';

export default {
	cognates: cognateReducer,
	page: pageReducer,
	title
};
