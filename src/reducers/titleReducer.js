const DEFAULT = 'Cognuts';
const capitalizeFirstChar = text => `${text.charAt(0).toUpperCase()}${text.substring(1)}`;

export default (state = DEFAULT, action = {}) => {
	switch (action.type) {
		case 'HOME':
			return DEFAULT;
		case 'COGNATE':
			return `${DEFAULT} - ${capitalizeFirstChar(action.payload.id)}`;
		default:
			return state;
	}
};
