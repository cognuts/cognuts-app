import * as React from 'react';
import Link from 'redux-first-router-link';
import logo from './logo-nut.png';
import './styles.scss';

const Header: React.SFC = () => (
  <header className='header'>
    <Link to={{ type: 'HOME'}}>
        <img src={logo} className="header__logo" alt="Cognuts logo"/>
    </Link>
  </header>
);

export default Header;
