import * as React from 'react';
import './styles.scss';

const Footer: React.SFC = () => (
  <footer className='footer'>Copyright &copy; 2019 Cognuts Limited.</footer>
);

export default Footer;
