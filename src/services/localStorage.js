import words from '../resources/words';

export const loadState = () => {
	try {
		const serializedState = localStorage.getItem('state');
		if (serializedState === null) {
			return undefined;
		}
		return JSON.parse(serializedState);
	} catch (err) {
		return undefined;
	}
};

export const updateCognates = persistedState => {
	if (persistedState) {
		if (persistedState.cognates.data.length < words.length) {
			return (persistedState = {
				...persistedState,
				cognates: {
					...persistedState.cognates,
					data: words.map(a => ({
						...a,
						...persistedState.cognates.data.find(b => b.en.id === a.en.id)
					}))
				}
			});
		} else {
			return persistedState;
		}
  }
  return persistedState;
};

export const saveState = state => {
	try {
		const serializedState = JSON.stringify(state);
		localStorage.setItem('state', serializedState);
	} catch (err) {
		//
	}
};
