import { applyMiddleware, compose, createStore, combineReducers } from 'redux';
import { connectRoutes } from 'redux-first-router'
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import monitorReducers from './enhancers/monitorReducers.tsx';
import reducers from './reducers/index';
import routesMap from './resources/routesMap';

export default function configureStore(preloadedState) {

  const { reducer, middleware, enhancer } = connectRoutes(routesMap);
  const rootReducer = combineReducers({...reducers, location: reducer});
  const middlewares = [middleware, logger, thunk];

  const enhancers = [
    monitorReducers,
    enhancer,
    applyMiddleware(...middlewares),
  ];

  const store = createStore(rootReducer, preloadedState, compose(...enhancers));

  return store;
}