
const monitorReducerEnhancer = createStore => (
  reducer: any,
  initialState: any,
  enhancer: any
) => {
  const monitoredReducer = (state: any, action: any) => {
    const start = performance.now();
    const newState = reducer(state, action);
    const end = performance.now();
    const diff = Math.round((end - start) * 100) / 100;
    /* tslint:disable */ console.log('reducer process time:', diff); /* tslint:enable */
    return newState;
  }

  return createStore(monitoredReducer, initialState, enhancer);
}

export default monitorReducerEnhancer;