const createCognatesForLanguages = (primaryLanguage, secondaryLanguage) => (
	primaryId,
	secondaryId
) => {
	const cognate = {
		[primaryLanguage]: { id: primaryId },
		[secondaryLanguage]: { id: secondaryId }
	};
	return cognate;
};

const createCognate = createCognatesForLanguages('en', 'pl');

const absence = createCognate('absence', 'absencja');
const absolute = createCognate('absolute', 'absolutny');
const absorb = createCognate('absorb', 'absorbować');
// const abstract = createCognate('abstract', 'abstrakcyjny');
// const absurd = createCognate('absurd', 'absurdalny');
// const academy = createCognate('academy', 'akademia');
// const accent = createCognate('accent', 'akcent');
// const accept = createCognate('accept', 'akceptować');
// const accommodate = createCognate('accommodate', 'akomodować');
// , abstract, absurd, academy, accent, accept, accommodate
const words = [absence, absolute, absorb];

export default words;