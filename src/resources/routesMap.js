import * as selectors from '../reducers/cognateReducer';
import { NOT_FOUND } from 'redux-first-router'

const fetchCognateThunk = (dispatch, getState) => {
    const { language = 'en', id } = getState().location.payload;
    const activeCognate = selectors.findByIdSelector(getState());

    let isFetched = false;
    if(activeCognate) {
        isFetched = activeCognate.en.hasOwnProperty('lexicalEntries');
    } else {
        dispatch({type: NOT_FOUND});
    }

    if (!isFetched) {
        dispatch({
            type: 'FETCH_COGNATE_REQUEST'
        });

        fetch(`https://cors-anywhere.herokuapp.com/https://od-api.oxforddictionaries.com/api/v1/entries/${language}/${id}`, {
            headers: {
                'accept': 'application/json',
                'app_id': '486beb53',
                'app_key': '4d15bd4c167a436f590ea5220775fed1'
            }
        })
            .then(response => response.json())
            .then(response => dispatch({
                payload: { response },
                type: 'FETCH_COGNATE_SUCCESS',

            }))
            .catch(err => dispatch({
                type: 'FETCH_COGNATE_ERROR',
            }))
    }
}

const routesMap = {
    COGNATE: {
        path: '/cognate/:id',
        thunk: fetchCognateThunk,
    },
    HOME: '/',
    NOT_FOUND: '/404'
}

export default routesMap;
  